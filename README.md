# altiroc-emulator

A simple piece of VHDL that will generate some 8b10b encoded packages for the HGTD altiroc emulator https://gitlab.cern.ch/atlas-hgtd/hgtd-peb/-/blob/master/Demonstration/module%20emulator%20v2/module_emulator_v2.pdf

## Getting started

cd scripts
source ./create_project.tcl
source ./run_implementation.tcl

All files should appear in output.

##
The altiroc emulator handles the Altiroc fastcmd, and generates (for now) the NSW formatted 8b10b frames with SOP/EOP and IDLE K-Characters.
The content of the generated data frame (1 for every trigger) is:

16 bytes:
L1ID0 L1ID1 L1ID2 XL1ID BCID0 BCID1 0x07 0x08 0x09 0x0A 0x0B 0x0C 0x0D 0x0E 0x0F 0x10

* L1ID is incremented on every trigger
* XL1ID is incremented on an ECR or overflow of L1ID
* BCID increments at 40 MHz and is cleared with BCR.

Other fastcmd commands are ignored.
